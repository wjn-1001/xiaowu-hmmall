package com.hmall.common.config;

import com.hmall.common.interceptor.FeignInterceptor;
import feign.Logger;
import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig {
    @Bean
    public Logger.Level feignConfiguration(){
        return Logger.Level.BASIC;
    }

    @Bean
    public RequestInterceptor requestInterceptor(){
        return new FeignInterceptor();
    }
}
