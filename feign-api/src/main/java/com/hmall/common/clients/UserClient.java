package com.hmall.common.clients;

import com.hmall.common.config.FeignConfig;
import com.hmall.common.dto.Address;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(value = "userservice",configuration = FeignConfig.class)
public interface UserClient {

        @GetMapping("/address/{addressId}")
        Address findAddressById(@PathVariable("addressId") Long addressId);
}
