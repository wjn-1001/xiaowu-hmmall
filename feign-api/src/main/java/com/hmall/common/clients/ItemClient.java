package com.hmall.common.clients;

import com.hmall.common.config.FeignConfig;
import com.hmall.common.dto.Item;
import com.hmall.common.dto.PageDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "itemservice",configuration = FeignConfig.class)
public interface ItemClient {

    @GetMapping("/item/list")
    PageDTO<Item> list(@RequestParam("page") Integer page,@RequestParam("size") Integer size);

    @GetMapping("/item/{id}")
    Item get(@RequestParam("id")Long id);

    @PutMapping("/item/stock/{itemId}/{num}")
    void down(@PathVariable("itemId") Long itemId, @PathVariable("num") Integer num);

    @PutMapping("/item")
    void updateItem(@RequestBody Item item);
}
