package com.hmall.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmall.common.dto.PageDTO;
import com.hmall.item.pojo.Item;

public interface IItemService extends IService<Item> {


    PageDTO<Item> select(Integer page, Integer size);

    Item getItemById(Long id);

    void insert(Item item);

    void upOrDown(Long id, Integer status);

    void updateItem(Item item);

    void delete(Long id);

    void down(Long itemId, Integer num);
}
