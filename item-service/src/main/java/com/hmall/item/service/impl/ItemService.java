package com.hmall.item.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmall.common.clients.ItemClient;
import com.hmall.common.dto.PageDTO;
import com.hmall.item.mapper.ItemMapper;
import com.hmall.item.pojo.Item;
import com.hmall.item.service.IItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ItemService extends ServiceImpl<ItemMapper, Item> implements IItemService {

    @Autowired
    private ItemMapper itemMapper;

    @Override
    public PageDTO<Item> select(Integer page, Integer size) {
        IPage iPage = new Page(page,size);
        itemMapper.selectPage(iPage,null);
        long total = iPage.getTotal();
        List list = iPage.getRecords();
        PageDTO pageDTO = new PageDTO<>(total,list);
        return pageDTO;
    }

    @Override
    public Item getItemById(Long id) {
        Item item = itemMapper.selectById(id);
        return item;
    }

    @Override
    public void insert(Item item) {
        itemMapper.insert(item);
    }

    @Override
    public void upOrDown(Long id, Integer status) {
        Item item = new Item();
        item.setId(id);
        item.setStatus(status);
        itemMapper.updateById(item);
    }

    @Override
    public void updateItem(Item item) {
        itemMapper.updateById(item);
    }

    @Override
    public void delete(Long id) {
        itemMapper.deleteById(id);
    }


    @Override
    @Transactional
    public void down(Long itemId, Integer num) {
        try {
            itemMapper.down(itemId,num);
        } catch (Exception e) {
            throw new RuntimeException("库存不足",e);
        }
    }

}
