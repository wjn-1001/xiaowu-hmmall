package com.hmall.item.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hmall.common.dto.PageDTO;
import com.hmall.item.pojo.Item;
import com.hmall.item.service.IItemService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("item")
public class ItemController {

    @Autowired
    private IItemService itemService;
    @Autowired
    private RabbitTemplate rabbitTemplate;


    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/list")
    public PageDTO<Item> list(Integer page,Integer size){
        PageDTO<Item> itemPageDTO = itemService.select(page,size);
        return itemPageDTO;
    }

    /**
     * 根据id查询商品
     */
    @GetMapping("/{id}")
    public Item selectById(@PathVariable Long id){
        Item item = itemService.getItemById(id);
        return item;
    }

    /**
     * 新增商品
     */
    @PostMapping
    public void insert(@RequestBody Item item){
        itemService.insert(item);
    }

    /**
     * 商品的和上架与下架
     */
    @PutMapping("/status/{id}/{status}")
    public void upOrDown(@PathVariable Long id,@PathVariable Integer status){
        itemService.upOrDown(id,status);
        if (status == 1){
            //上架
            rabbitTemplate.convertAndSend("mall.exchange","add",id);
        }else {
            rabbitTemplate.convertAndSend("mall.exchange","delete",id);
        }
    }

    /**
     * 修改商品
     */
    @PutMapping
    public void updateItem(@RequestBody Item item){
        itemService.updateItem(item);
    }

    /**
     * 根据id删除商品
     */
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        itemService.delete(id);
    }

    @PutMapping("/stock/{itemId}/{num}")
    public void down(@PathVariable Long itemId,@PathVariable Integer num){
        itemService.down(itemId,num);
    }

}
