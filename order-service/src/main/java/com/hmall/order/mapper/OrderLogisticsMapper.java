package com.hmall.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmall.order.pojo.OrderDetail;
import com.hmall.order.pojo.OrderLogistics;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderLogisticsMapper extends BaseMapper<OrderLogistics> {
}
