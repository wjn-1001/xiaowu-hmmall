package com.hmall.order.pojo;

import lombok.Data;

@Data
public class RequestParams {
    private Integer num;//购买数量
    private Integer paymentType;//付款方式
    private Long addressId;//收货人地址Id
    private Long itemId;//商品id
}
