package com.hmall.order.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class DelayConfig {

    //声明一个普通队列
    @Bean
    public Queue delayQueue(){
        return QueueBuilder.durable("delay.ttl.queue")
                .ttl(10000)
                .deadLetterExchange("delay.exchange")
                .deadLetterRoutingKey("delay")
                .build();
    }

    //声明一个普通交换机
    @Bean
    public DirectExchange delayExchange(){
        return new DirectExchange("delay.ttl.exchange");
    }

    //绑定普通交换机与普通队列
    @Bean
    public Binding bindingDelayQueue(Queue delayQueue,DirectExchange delayExchange){
        return BindingBuilder.bind(delayQueue).to(delayExchange).with("longtime");
    }
}
