package com.hmall.order.listener;

import com.hmall.common.clients.ItemClient;
import com.hmall.common.dto.Item;
import com.hmall.order.mapper.OrderDetailMapper;
import com.hmall.order.mapper.OrderMapper;
import com.hmall.order.pojo.Order;
import com.hmall.order.pojo.OrderDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DelayListener {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderDetailMapper orderDetailMapper;
    @Autowired
    private ItemClient itemClient;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "delay.queue",durable = "true"),
            exchange = @Exchange(name = "delay.exchange"),
            key = "delay"))
    public void delay(Long id){
        log.info("收到消息：{}",id +":"+ System.currentTimeMillis());
        //根据id查询订单信息
        Order order = orderMapper.selectById(id);
        //获取订单状态
        Integer status = order.getStatus();
        //订单未支付
        if (1 == status){
            //设置状态为取消并修改
            order.setStatus(5);
            orderMapper.updateById(order);
            //通过订单id查询到订单详情
            OrderDetail orderDetail = orderDetailMapper.selectByOrderId(id);
            //获取商品购买的数量
            Integer num = orderDetail.getNum();
            //获取到商品的id
            Long itemId = orderDetail.getItemId();
            //查询商品信息
            Item item = itemClient.get(itemId);
            //获取商品库存
            Integer stock = item.getStock();
            //将现有库存与购买的数量相加
            Integer total = stock + num;
            //修改库存
            item.setStock(total);
            itemClient.updateItem(item);
        }
    }
}
