package com.wujiani.search.web;


import com.hmall.common.dto.PageDTO;
import com.wujiani.search.pojo.ItemDoc;
import com.wujiani.search.pojo.RequestParams;
import com.wujiani.search.service.ISearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/search")
public class SearchController {


    @Autowired
    private ISearchService searchService;

    @GetMapping("/suggestion")
    public List<String> suggestion(String key){
        return searchService.getSuggestion(key);
    }

    @PostMapping("/filters")
    public Map<String,List<String>> filters(@RequestBody RequestParams requestParams){
        return searchService.getFilters(requestParams);
    }

    @PostMapping("/list")
    public PageDTO<ItemDoc> list(@RequestBody RequestParams requestParams){
        return searchService.list(requestParams);
    }
}
