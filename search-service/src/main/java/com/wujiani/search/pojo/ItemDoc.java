package com.wujiani.search.pojo;

import com.hmall.common.dto.Item;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@Data
@NoArgsConstructor
public class ItemDoc {
    private Long id;
    private String name;
    private Long price;
    private String image;
    private String category;
    private String brand;
    private Integer sold;
    private Integer commentCount;
    private List<String> suggestion;

    private boolean isAD;


    public ItemDoc(Item item) {
        this.id = item.getId();
        this.name = item.getName();
        this.price = item.getPrice();
        this.image = item.getImage();
        this.category = item.getCategory();
        this.brand = item.getBrand();
        this.sold = item.getSold();
        this.commentCount = item.getCommentCount();
        this.suggestion = Arrays.asList(this.brand,this.category);
    }
}
