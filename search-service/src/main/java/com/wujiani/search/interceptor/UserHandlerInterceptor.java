package com.wujiani.search.interceptor;


import com.wujiani.search.thread.Local;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserHandlerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String header = request.getHeader("authorization");
        if (header == null || "".equals(header)){
            response.setStatus(403);
            return false;
        }
        Long userId = Long.valueOf(header);
        Local.setUserId(userId);
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        Local.removeUserId();
    }
}
