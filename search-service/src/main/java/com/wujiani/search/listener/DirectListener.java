package com.wujiani.search.listener;


import com.alibaba.fastjson.JSON;
import com.hmall.common.clients.ItemClient;
import com.hmall.common.dto.Item;
import com.wujiani.search.pojo.ItemDoc;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
@Slf4j
@Component
public class DirectListener {

    @Autowired
    private ItemClient itemClient;
    @Autowired
    private RestHighLevelClient client;

    //上架
    @RabbitListener(queues = "add.queue")
    public void listenerAddQueue(Long id){
        log.info("上架，{}",id);
        try {
            //获取增加的数据
            Item item = itemClient.get(id);
            //将数据转为文档数据
            ItemDoc itemDoc = new ItemDoc(item);
            //将文档转为json格式
            String json = JSON.toJSONString(itemDoc);
            //发送新增文档请求
            IndexRequest request = new IndexRequest("item").id(itemDoc.getId().toString());
            request.source(json, XContentType.JSON);
            client.index(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //下架
    @RabbitListener(queues = "delete.queue")
    public void listenerDeleteQueue(Long id){
        try {
            DeleteRequest request = new DeleteRequest("item", id.toString());
            client.delete(request,RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
