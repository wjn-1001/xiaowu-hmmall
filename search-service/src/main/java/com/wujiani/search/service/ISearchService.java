package com.wujiani.search.service;

import com.hmall.common.dto.PageDTO;
import com.wujiani.search.pojo.ItemDoc;
import com.wujiani.search.pojo.RequestParams;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

public interface ISearchService {
    List<String> getSuggestion(String key);

    Map<String, List<String>> getFilters(RequestParams requestParams);

    PageDTO<ItemDoc> list(RequestParams requestParams);
}
