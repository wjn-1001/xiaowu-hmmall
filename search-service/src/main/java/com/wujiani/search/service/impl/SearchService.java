package com.wujiani.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.hmall.common.dto.PageDTO;
import com.wujiani.search.pojo.ItemDoc;
import com.wujiani.search.pojo.RequestParams;
import com.wujiani.search.service.ISearchService;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SearchService implements ISearchService {
    @Autowired
    private RestHighLevelClient client;

    @Override
    public List<String> getSuggestion(String key) {
        try {
            //准备request
            SearchRequest request = new SearchRequest("item");
            //DSL 自动补全
            request.source().suggest(new SuggestBuilder().addSuggestion("suggestions", SuggestBuilders.completionSuggestion("suggestion")
                    .prefix(key).skipDuplicates(true).size(10)));
            SearchResponse search = client.search(request, RequestOptions.DEFAULT);
            //处理结果
            Suggest suggest = search.getSuggest();
            CompletionSuggestion suggestions = suggest.getSuggestion("suggestions");
            List<CompletionSuggestion.Entry.Option> options = suggestions.getOptions();
            List<String> list = new ArrayList<>();
            for (CompletionSuggestion.Entry.Option option : options) {
                String text = option.getText().toString();
                list.add(text);
            }
            return list;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public Map<String, List<String>> getFilters(RequestParams requestParams) {
        try {
            SearchRequest request = new SearchRequest("item");
            queryFilter(requestParams,request);
            //设置聚合条件
            request.source().size(0);
            //分类
            request.source().aggregation(AggregationBuilders.terms("category").field("category").size(20));
            //品牌
            request.source().aggregation(AggregationBuilders.terms("brand").field("brand").size(20));
            //发起请求
            SearchResponse response = client.search(request, RequestOptions.DEFAULT);
            //处理结果
            Map<String,List<String>> map = new HashMap<>();
            Aggregations aggregations = response.getAggregations();
            //分类
            Terms category = aggregations.get("category");
            List<? extends Terms.Bucket> buckets = category.getBuckets();
            List<String> categoryList = new ArrayList<>();
            for (Terms.Bucket bucket : buckets) {
                String keyAsString = bucket.getKeyAsString();
                categoryList.add(keyAsString);
            }
            map.put("category",categoryList);
            //品牌
            Terms brand = aggregations.get("brand");
            List<? extends Terms.Bucket> buckets1 = brand.getBuckets();
            List<String> brandList = new ArrayList<>();
            for (Terms.Bucket bucket : buckets1) {
                String keyAsString = bucket.getKeyAsString();
                brandList.add(keyAsString);
            }
            map.put("brand",brandList);
            return map;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public PageDTO<ItemDoc> list(RequestParams requestParams) {
        try {
            SearchRequest request = new SearchRequest("item");
            //设置搜索条件
            queryFilter(requestParams,request);
            //分页
            int page = requestParams.getPage();
            int size = requestParams.getSize();
            request.source().from((page - 1)*size).size(size);
            SearchResponse searchResponse = client.search(request, RequestOptions.DEFAULT);
            //排序
            String sortBy = requestParams.getSortBy();
            if ("sold".equals(sortBy)) {
                request.source().sort(sortBy, SortOrder.DESC);
            } else if ("price".equals(sortBy)) {
                request.source().sort(sortBy, SortOrder.ASC);
            }
            //高亮
            request.source().highlighter(new HighlightBuilder().field("name").requireFieldMatch(false));
            //解析数据
            SearchHits hits = searchResponse.getHits();
            long total = hits.getTotalHits().value;
            SearchHit[] hitsHits = hits.getHits();
            List<ItemDoc> list = new ArrayList<>();
            for (SearchHit hitsHit : hitsHits) {
                String source = hitsHit.getSourceAsString();
                ItemDoc itemDoc = JSON.parseObject(source, ItemDoc.class);
                Map<String, HighlightField> highlightFields = hitsHit.getHighlightFields();
                if (highlightFields != null && highlightFields.size() > 0){
                    HighlightField name = highlightFields.get("name");
                    String string = name.getFragments()[0].toString();
                    itemDoc.setName(string);

                }
                list.add(itemDoc);
            }
            return new PageDTO<>(total,list);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private static void queryFilter(RequestParams requestParams, SearchRequest request) {
        //设置搜索
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        //key
        String key = requestParams.getKey();
        if (key == null || "".equals(key)) {
            boolQuery.must(QueryBuilders.matchAllQuery());
        } else {
            boolQuery.must(QueryBuilders.matchQuery("all", key));
        }
        //brand
        String brand = requestParams.getBrand();
        if (brand != null && !brand.equals("")) {
            boolQuery.filter(QueryBuilders.termQuery("brand", brand));
        }
        //category
        String category = requestParams.getCategory();
        if (category != null && !category.equals("")) {
            boolQuery.filter(QueryBuilders.termQuery("category", category));
        }
        //price
        Long minPrice = requestParams.getMinPrice();
        Long maxPrice = requestParams.getMaxPrice();
        if (minPrice != null && maxPrice != null) {
            boolQuery.filter(QueryBuilders.rangeQuery("price").gte(minPrice * 100).lte(maxPrice * 100));
        }
        //算分查询
        FunctionScoreQueryBuilder functionScoreQuery = QueryBuilders.functionScoreQuery(boolQuery, new FunctionScoreQueryBuilder.FilterFunctionBuilder[]{
                new FunctionScoreQueryBuilder.FilterFunctionBuilder(QueryBuilders.termQuery("isAD", true), ScoreFunctionBuilders.weightFactorFunction(10))
        });
        request.source().query(functionScoreQuery);
    }
}
