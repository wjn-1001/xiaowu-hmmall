package com.wujiani.search.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DirectConfig {

    //声明交换机类型
     @Bean
    public DirectExchange itemExchange(){
         return new DirectExchange("mall.exchange");
     }

     //声明第一个队列--上架
    @Bean
    public Queue addQueue(){
         return new Queue("add.queue");
    }

    //声明第二个队列--下架
    @Bean
    public Queue deleteQueue(){
         return new Queue("delete.queue");
    }

    //绑定上架交换机
    @Bean
    public Binding bindingAddQueue(Queue addQueue, DirectExchange itemExchange){
         return BindingBuilder.bind(addQueue).to(itemExchange).with("add");
    }

    //绑定下架交换机
    @Bean
    public Binding bindingDeleteQueue(Queue deleteQueue,DirectExchange itemExchange){
         return BindingBuilder.bind(deleteQueue).to(itemExchange).with("delete");
    }


}
