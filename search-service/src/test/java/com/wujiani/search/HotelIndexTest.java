package com.wujiani.search;

import com.alibaba.fastjson.JSON;
import com.hmall.common.clients.ItemClient;
import com.hmall.common.dto.Item;
import com.hmall.common.dto.PageDTO;
import com.wujiani.search.pojo.ItemDoc;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;

import static com.wujiani.search.constants.ItemSearchConstant.MAPPING_TEMPLATE;

@SpringBootTest
public class HotelIndexTest {
    @Autowired
    private RestHighLevelClient client;

    @Autowired
    private ItemClient itemClient;


    @Test
    //创建索引库
    public void createIndex() throws IOException {
        CreateIndexRequest request = new CreateIndexRequest("item");
        request.source(MAPPING_TEMPLATE, XContentType.JSON);
        client.indices().create(request, RequestOptions.DEFAULT);
    }


    @Test
    void testBulkRequest() throws IOException {
        BulkRequest request = new BulkRequest();
        // 批量查询
        PageDTO<Item> dtoList = itemClient.list(1, 500);
        Long total = dtoList.getTotal();

        int page = (int) (total % 1000 == 0 ? total / 1000 : total / 1000 + 1);
        int i = 1;
        for (int j = 1; j <= page; j++) {
            PageDTO<Item> list = itemClient.list(i, 1000);
            List<Item> items = list.getList();
            for (Item item : items) {
                ItemDoc itemDoc = new ItemDoc(item);
                request.add(new IndexRequest("item")
                        .id(itemDoc.getId().toString())
                        .source(JSON.toJSONString(itemDoc), XContentType.JSON));
            }
            i++;
            client.bulk(request, RequestOptions.DEFAULT);
        }
    }

}
